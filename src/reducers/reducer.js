import { AUTH__USER, IS__ADMIN } from '../actions'


const initialState = {
    user: null,
    isAdmin: true,
    basket: 0,
    products: []
}

const reducer = (state=initialState, action) => {
    
    switch(action.type) {
        case AUTH__USER:
            return { ...state, user : action.payload } 

        case IS__ADMIN:
            return { ...state, isAdmin : action.payload } 

        default:
            return state
    }
        

}

export default reducer
