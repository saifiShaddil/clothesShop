import firebase from 'firebase';

const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyDvpELTQVsVnKMeZxEGwWn8mnKi_ZvjJ_M",
  authDomain: "clothes-d6b1c.firebaseapp.com",
  projectId: "clothes-d6b1c",
  storageBucket: "clothes-d6b1c.appspot.com",
  messagingSenderId: "779368428632",
  appId: "1:779368428632:web:ebf805d17b61adbb13062d",
  measurementId: "G-K6NCQ8JM93"
});

const db = firebaseApp.firestore()
const auth = firebase.auth()
const storage = firebase.storage()

export { auth, db, storage }

//npm i firebase righthere