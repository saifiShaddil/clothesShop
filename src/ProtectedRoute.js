import React from "react";
import { Redirect, Route } from "react-router";

const ProtectedRoute = ({ component: Comp, logger , path, ...rest }) => {
  console.log(logger);
  return (
    <Route
      path={path}
      {...rest}
      render={(props) => {
        // console.log(props);
        return logger ? (
          <Comp {...props} />
        ) : (
          <Redirect to="/" />
        );
      }}
    />
  );
};

export default ProtectedRoute
