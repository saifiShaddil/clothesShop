import React, { useEffect } from "react";
import { connect, useDispatch } from "react-redux";
import { Route, Switch } from "react-router";
import { AUTH__USER } from "./actions";
import Login from "./Components/Authentication/Login";
import Home from "./Components/Home/Home";
import Checkout from './Pages/Checkout/Checkout'
import { auth } from "./config/firebase";
import Admin from './Pages/Admin'
import Dashboard from './Pages/Admin/Dashboard/Dashboard'
import ProtectedRoute from "./ProtectedRoute";

function App({user, isadmin}) {

  const dispatch = useDispatch();

  // console.log(isadmin);

  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged((authuser) => {
      if (authuser) {
        dispatch({
          type: AUTH__USER,
          payload: authuser,
        });
      }
      else {
        dispatch({
          type: AUTH__USER,
          payload: "Shaddil",
        });
      }
    });
    return () => {
      unsubscribe();
    };
  }, [dispatch]);
  return (
    <>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/checkout" component={Checkout} />
        <Route exact path="/admin" component={Admin} />
        <ProtectedRoute exact path="/admin-dashboard" logger={isadmin} component={Dashboard} />
        {/* <Route exact path="/products" component={Products} /> */}
        {/* <ProtectedRoute path="/" isLogged={user.user} component={Home} /> */}
      </Switch>
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    isadmin : state.isAdmin
  };
};

export default connect(mapStateToProps)(App);
