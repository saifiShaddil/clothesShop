import React from 'react'
import { connect } from 'react-redux'
import AdminLogin from './AdminLogin'

const index = ({user}) => {
    return (
        <div className="admin">
            <AdminLogin />
        </div>
    )
}

const mapStateToProps = state => ({
    user: state.user
})

export default connect(mapStateToProps)(index)
