import React from "react";
import { connect } from "react-redux";
import Navbar from "../../Components/Header/Navbar/Navbar";
import './checkout.css'

const Checkout = (basket) => {
    console.log(basket.basket);

  return (
    <>
      <Navbar />
      <div className="checkout">
        <div className="checkout__left">
          <img className="checkout__ad" src="" alt="add" />
          {basket?.basket === 0 ? (
            <div>
              <h2>Your Shopping Basket is Empty</h2>
              <p>You have not added anything to basket yet!</p>
            </div>
          ) : (
            <div>
              <h2 className="checkout__title">Your Basket</h2>

              {/* list of all checkout products */}
              {/* {basket?.map((item) => (
                <CheckoutProduct
                  id={item.id}
                  title={item.title}
                  image={item.image}
                  price={item.price}
                  rating={item.rating}
                />
              ))} */}
            </div>
          )}
        </div>
        {basket?.length > 0 && (
          <div className="checkout__right">
            <h2>Subtotal</h2>
            {/* <Subtotal /> */}
          </div>
        )}
      </div>
    </>
  );
};
const mapStateToProps = (state) => {
    return {
      basket: state.basket,
    };
  };
export default connect(mapStateToProps, null)(Checkout)
