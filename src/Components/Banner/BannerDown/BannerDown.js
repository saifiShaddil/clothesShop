import React from "react";
import { BiLeftArrowAlt, BiRightArrowAlt } from "react-icons/bi";
import { Link } from "react-router-dom";
import "./index.css";

const BannerDown = () => {
  return (
    <div
      id="myCarousel"
      className="carousel slide carousel-fade"
      data-ride="carousel"
    >
      <div className="carousel-inner">
        <div className="carousel-item active">
          <div className="mask flex-center">
            <div className="container mx-auto col-10">
              <div className="row align-items-center">
                <div className="col-md-8 ml-md-4 col-12 order-md-1 order-2">
                  <h4>Latest Summer Wear</h4>
                  <p className="d-none d-md-block">
                    This has many features that are simply awesome. The phone
                    comes with a 3.50-inch display with a resolution of 320x480
                    pixels.
                  </p>
                  <br /> <Link to="/checkout">BUY NOW</Link>
                </div>
                <div className="col-md-4 col-12 order-md-2 order-1">
                  <img
                    src="https://images.unsplash.com/photo-1621201638603-457601730283?ixid=MnwxMjA3fDB8MHx0b3BpYy1mZWVkfDd8UzRNS0xBc0JCNzR8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
                    className="mx-auto"
                    alt="slide"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="carousel-item">
          <div className="mask flex-center">
            <div className="container mx-auto col-10">
              <div className="row align-items-center">
                <div className="col-md-8 col-12 order-md-1 order-2">
                  <h4>New Collections</h4>
                  <p className="d-none d-md-block">
                    This has many features that are simply awesome.The phone
                    comes with a 3.50-inch display with a resolution of 320x480
                    pixels.
                  </p>{" "}
                  <br /> <Link to="/checkout">BUY NOW</Link>
                </div>
                <div className="col-md-4 col-12 order-md-2 order-1">
                  <img
                    src="https://images.unsplash.com/photo-1567719367005-f4a24fcc8794?ixid=MnwxMjA3fDB8MHx0b3BpYy1mZWVkfDMzfFM0TUtMQXNCQjc0fHxlbnwwfHx8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
                    className="mx-auto"
                    alt="slide"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <a
        className="carousel-control-prev"
        href="#myCarousel"
        role="button"
        data-slide="prev"
        style={{ position: "absolute" }}
      >
        
        <span
          className="carousel-control-prev-icon"
          aria-hidden="true"
        ></span>{" "}
        <span className="sr-only"><BiLeftArrowAlt /></span>
      </a>
      <a
        className="carousel-control-next"
        href="#myCarousel"
        role="button"
        data-slide="next"
        style={{ position: "absolute", right: 0}}
      >
        
        <span
          className="carousel-control-next-icon"
          aria-hidden="true"
        ></span>
        <span className="sr-only"><BiRightArrowAlt /></span>
      </a>
    </div>
  );
};

export default BannerDown;
