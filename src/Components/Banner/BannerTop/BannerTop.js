import React from "react";
import ImgBanner from '../../../Images/bg1.jpg'
import './index.css'


const BannerTop = () => {
  return (
    <div className="bannertop ">
      <img src={ImgBanner} alt="BannerTop" />
      <main className="banner__main">
        
      </main>
    </div>
  );
};

export default BannerTop;
