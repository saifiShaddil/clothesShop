import React from "react";
import Navbar from "../Header/Navbar/Navbar";
import BannerTop from '../Banner/BannerTop/BannerTop'
import BannerDown from '../Banner/BannerDown/BannerDown'
import './home.css'
import Product from "../Product/Product";
import  Carousel  from 'react-elastic-carousel'
import { Typography } from "@material-ui/core";


const Home = () => {
  return (
    <>
      <Navbar />
      <section className="banner--wrapper">
        <BannerTop />
        {/* <BannerDown /> */}
      </section>
      <main className="home-product-area">
        <Typography variant="h4" component="h2" style={{ marginTop: '1.5em', marginBottom: '1.5em', color: 'gray', textDecoration: 'underline' }} >Trending Poducts</Typography>
        <Product />
      </main>
      
    </>
  );
};

export default Home;
