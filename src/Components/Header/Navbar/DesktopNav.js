import React from "react";
import { Link } from "react-router-dom";


const DesktopNav = () => {
  return (
    <div className="navbar__nav">
      <ul className="nav__items">
        <li className="nav__item">
          <Link to="/products" className="nav__link">
            Products
          </Link>
        </li>
        <li className="nav__item">
          <Link to="/checkout" className="nav__link">
            Checkout
          </Link>
        </li>
        <li className="nav__item">
          <Link to="/contact" className="nav__link btn">
            Contact
          </Link>
        </li>
      </ul>
    </div>
  );
};

export default DesktopNav;
