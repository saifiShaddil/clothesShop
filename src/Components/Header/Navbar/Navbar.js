import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { FiMenu } from "react-icons/fi";
import { AiOutlineClose } from "react-icons/ai";
import "./navbar.css";
import DesktopNav from './DesktopNav'

const Navbar = (isAdmin, user) => {
  const [width, setWidth] = useState(window.innerHeight);
  const [scroll, setScroll] = useState(window.scrollY)
  const [open, setOpen] = useState(false)
  const [navbarToggle, setnavbarToggle] = useState(false)

  useEffect(() => {
    window.addEventListener("resize", () => setWidth(window.innerWidth));
    window.addEventListener("scroll", () => setScroll(window.scrollY))
    // console.log(width);
  }, []);



  return (
    <>
      <header className={scroll >= 120 && "active"}>
        <nav className="navbar">
          <Link to="/" className="navbar__brand">
            FashionIsta
          </Link>
          { width > "768" ? <DesktopNav /> : (
            <>
              {!open ? <FiMenu onClick={() => setOpen(!open)}/> : <AiOutlineClose onClick={() => setOpen(!open)} />}
              <div className={open ? "sidebar active" : "sidebar"} >
                <ul className="side__menu">
                  {/* <li className="side__item"></li> */}
                  <li className="side__item"><Link className="side__link">Home</Link></li>
                  <li className="side__item"><Link className="side__link">Products</Link></li>
                  <li className="side__item"><Link className="side__link">Contact</Link></li>
                </ul>
              </div>
            </>
          )}
        </nav>
      </header>
    </>
  );
};

const mapStateToprops = (state) => {
  return {
    user: state.user,
    isAdmin: state.isAdmin,
  };
};

export default connect(mapStateToprops)(Navbar);
