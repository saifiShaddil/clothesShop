import { Button, Card, IconButton, Typography, CardActions, CardContent, CardMedia, } from "@material-ui/core";
import React, { useState } from "react";
import './style.css'
import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt';
import DeleteIcon from '@material-ui/icons/Delete';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import AttachMoneySharpIcon from '@material-ui/icons/AttachMoneySharp';


const ProductCard = ({ title, img, price, id, des, category }) => {
  const [viewproduct, setViewproduct] = useState(true)

  const addToCart = (e) => {
    e.preventDefault()

  }
  // console.log(Carousel);
  const showModal = () => {

  }
  

  return (
    <>
      <Card className='card'>
      <CardMedia height="140" className='media' image={img} title={title} component="img" />
      <div className='overlay'>
        <Typography variant="h6">{title}</Typography>
        {/* <Typography variant="body2">{moment(post.createdAt).fromNow()}</Typography> */}
      </div>
      <div className='overlay2'>
        <Button style={{ color: 'white' }} size="small"><MoreHorizIcon fontSize="default" /></Button>
      </div>
      <div className='details'>
        <Typography variant="body2" color="textSecondary" component="h2">{category}</Typography>
      </div>
      <Typography className='title' gutterBottom variant="h5" component="h3">{title}</Typography>
      {/* <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">{des}</Typography>
      </CardContent> */}
      <CardActions className='cardActions'>
        <Button size="small" color="primary">More Info </Button>
        {/* <Button size="small" color="primary"><DeleteIcon fontSize="small" /> Delete</Button> */}
      </CardActions>
    </Card>
    </>
  );
};

export default ProductCard;
