import React, { useEffect } from "react";
import { useState } from "react";
import { db } from "../../config/firebase";
import ProductCard from "./ProductCard/ProductCard";
import './product.css'
import {product_info} from '../../products_info'

const Product = () => {
  const [prod, setProd] = useState();
  const [data, setData] = useState()
 

  function getProducts() {
    db.collection("products").onSnapshot((snapshot) => {
      setProd(snapshot.docs.map((doc) => doc.data()));
    });
}

  // async function fetchData(){
  //   const data = await fetch('https://fakestoreapi.com/products?limit=10')
  //   const res = await data.json()
  //   setData(res)
  // }
   
  useEffect(() => {
    getProducts();
    // fetchData()
  }, []);

  // console.log(prod);

  return (
    <section className="container-fluid">
      <div className="card-wrapper">
        {product_info
          ? product_info.map((item, index) => {
              return (
                <ProductCard
                  key={index}
                  id={item.id}
                  title={item.title}
                  price={item.price}
                  category={item.category}
                  des={item.description}
                  rating={item.rating}
                  img={item.image}
                />
              );
            })
          : null}
          
          {/* <ProductCard /> */}
      </div>
    </section>
  );
};

export default Product;
