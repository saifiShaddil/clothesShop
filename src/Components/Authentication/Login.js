import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import "./login.css";
import { auth } from "../../config/firebase";

function Login() {
  const history = useHistory();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const login = (event) => {
    event.preventDefault(); //this stop the refresh of page
    //login logic
    auth
      .signInWithEmailAndPassword(email, password)
      .then((auth) => {
        //logged in ..redirect to home page
        history.push("/");
      })
      .catch(e => alert(e.message));
  };

  const register = (event) => {
    event.preventDefault();
    // register logic
    auth
      .createUserWithEmailAndPassword(email, password)
      .then((auth) => {
        //create a user and logged in ..redirect to home page
        history.push("/");
      })
      .catch((e) => alert(e.message));
  };

  return (
    <div className="login">
      <Link to="/">
        <img src="#logo" className="login__Logo" alt="logo" />
      </Link>

      <div className="login__container">
        <h1>Login</h1>
        <form>
          <h5>E-mail</h5>
          <input
            value={email}
            onChange={(event) => setEmail(event.target.value)}
            type="email"
          />
          <h5>Password</h5>
          <input
            value={password}
            onChange={(event) => setPassword(event.target.value)}
            type="password"
          />
          <button onClick={login} type="submit" className="login__signInButton">
            Login
          </button>
        </form>
        <p>
          By signing-in you aree to Amazon's Condition of see and Privacy Policy
          Notice, Our Cookies Notice and other Notices.
        </p>
        <button onClick={register} className="login__registerButton">
          Create New Account
        </button>
      </div>
    </div>
  );
}

export default Login;

// fire >autentication > signIn Method > email password enable it
