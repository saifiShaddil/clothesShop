import { Paper, Typography, TextField, Button } from '@material-ui/core'
import React from 'react'

const Form = () => {
    return (
        <Paper className="dashboard">
        <form noValidate onSubmit={handleSubmit} clasName="form">
          <Typography variant="h6">Add New Product</Typography>
          <TextField name="title"  label="Title"  ></TextField>
          <TextField name="price" variant="outlined"></TextField>
          <TextField name="description" variant="outlined"></TextField>
          <TextField name="tag" variant="outlined"></TextField>
          <input type="file" name="file" className="fileInput" />
          <Button className="buttonSubmit" type='submit' color="primary" variant="contained" >Add Product</Button>
        </form>
      </Paper>
    )
}

export default Form
